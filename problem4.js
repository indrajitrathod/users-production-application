// The function returns the usernames sorted in alphabetic order
const sortedUsersNames = (users) => {
    let user_emails = [];

    if (Array.isArray(users) && users.length) {

        for (const user of users) {

            if (typeof user === 'object' && !Array.isArray(user))
                user_emails.push(user.email);
        }
    }
    return user_emails;
}

module.exports = sortedUsersNames;