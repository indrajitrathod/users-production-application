# Users - Production Application

### A simple **user production application** project, which a list of users from a production application.

## This project mainly solves 6 problems

* #### Problem 1
  The owner of the company wants to find the details of the user with id 100.<br/> 
  Help the dealer find out which user has an id of 100 by calling a function that will return the data for that user.<br/>
  Then log the car's name, email, and gender in the console log in the format of:<br/>
  `*User name goes here* is a *user gender goes here* and can be contacted on *user email goes here*`

* #### Problem 2
  The owner of the company wants the information on the last user in the list. <br/>
  Execute a function to find what the name and email of the last user in the inventory is.<br/>
  Log the name and email into the console in the format of:</br> 
`Last user is *user name goes here* and can be contacted on *user email goes here`

* #### Problem 3
    The marketing team wants the users listed alphabetically on the website by last name.<br/> 
    Execute a function to Sort all the user's names into alphabetical order and log the results in the console as it was returned.

* #### Problem 4
  The marketing team needs all the emails from every user on the list. Execute a function that will return an array from the user data containing only the emails and log the result in the console as it was returned.

* #### Problem 5
  The marketing manager needs to find out how many users are male. Find out how many users are male and return the array of names and emails and log their length.

* #### Problem 6
  The metrics team is interested in seeing how many users fall into which gender.  Execute a function and return an array of arrays where each array contains the user information of a particular gender in the order Male, Female, Polygender, Bigender, Genderqueer, Genderfluid, Agender.  Once you have the array, use JSON.stringify() to show the results of the array in the console.
