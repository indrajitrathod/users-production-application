// The function returns the usernames sorted in alphabetic order
const usersByGender = (users) => {
    const usersGroupedByGender = [];

    const genderOrder = ['Male', 'Female', 'Polygender', 'Bigender', 'Genderqueer', 'Genderfluid', 'Agender'];

    if (Array.isArray(users)) {

        for (const gender of genderOrder) {
            const genderGroup = [];

            for (const user of users) {

                if (typeof user === 'object' && !Array.isArray(user)) {
                    if (user.gender === gender) {
                        genderGroup.push(user);
                    }
                }
            }

            usersGroupedByGender.push(genderGroup);
        }

    }
    return usersGroupedByGender;
}

module.exports = usersByGender;