// The function returns the usernames sorted in alphabetic order
const sortedUsersNames = (users) => {
    let lastNames = [];

    if (Array.isArray(users)) {

        for (const user of users) {

            if (typeof user === 'object' && !Array.isArray(user))
                lastNames.push(user.last_name);
        }
    }
    return lastNames.sort();
}

module.exports = sortedUsersNames;