const assert = require('chai').assert;
const { describe } = require('mocha');

const users = require('../users');
const problem2 = require('../problem2');

describe('problem2', () => {

    it('Function should return last user object', () => {
        const result = problem2(users);
        const expected = {
            id: 1000,
            first_name: 'Gwynne',
            last_name: 'Stoffels',
            email: 'gstoffelsrr@mashable.com',
            gender: 'Polygender',
            ip_address: '217.57.7.83'
        }
        
        assert.deepEqual(problem2(users), expected);

        console.log(`Last user is ${result.first_name} and can be contacted on ${result.email}`);
    });

    it('Function shouldn\'t throw error when users data is not an array', () => {
        assert.isEmpty(problem2({ id: 100, name: "Test", length: 10 }, 100));
        assert.isObject(problem2({ id: 100, name: "Test", length: 10 }, 100));
        assert.isEmpty(problem2(new String("hello"), 100), 100);
        assert.isObject(problem2(new String("hello"), 100), 100);

    });

    it('Function shouldn\'t throw error when users data is not passed', () => {
        assert.isEmpty(problem2([], 100));
        assert.isObject(problem2([], 100));
        assert.isEmpty(problem2());
        assert.isObject(problem2());
    });

    it('Function shouldn\'t throw error when users data is not in proper array of object format', () => {

        const users = [{
            "id": 100,
            "first_name": "Corrina",
            "last_name": "Nussey",
            "email": "cnussey2r@examiner.com",
            "gender": "Genderfluid",
            "ip_address": "33.102.124.105"
        },
        [
            "id", 100,
            "first_name", "Corrina",
            "last_name", "Nussey",
            "email", "cnussey2r@examiner.com",
            "gender", "Genderfluid",
            "ip_address", "33.102.124.105"
        ],
        {
            "id": 102,
            "first_name": "Norrie",
            "last_name": "Kezourec",
            "email": "nkezourec2t@zimbio.com",
            "gender": "Polygender",
            "ip_address": "152.189.93.138"
        }];

        const result = problem2(users);
        assert.isObject(result);

    });
});