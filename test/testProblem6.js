const assert = require('chai').assert;
const { describe } = require('mocha');

const users = require('../users');
const problem6 = require('../problem6');

describe('problem6', () => {

    it('Function should list male user names & emails', () => {
        const result = problem6(users);
        console.log(JSON.stringify(result));
    });

    it('Function should return empty array when users is not passed', () => {
        assert.isArray(problem6());
        assert.isEmpty(problem6());
    });

    it('Return shouldn\'t be affected with more arguments', () => {
        const result = problem6(users);
        assert.deepEqual(problem6(users, 5478415, 'Dumped string'), result);
    });

    it('Function shouldn\'t throw error when users data is not an array', () => {
        assert.isEmpty(problem6({ id: 33, name: "Test", length: 10 }));
        assert.isArray(problem6({ id: 33, name: "Test", length: 10 }));
        assert.isEmpty(problem6(new String("Mountblue")));
        assert.isArray(problem6(new String("mountblue")));
    });

    it('Function shouldn\'t throw error when users data is not in proper array of object format', () => {

        const users = [{
            "id": 100,
            "first_name": "Corrina",
            "last_name": "Nussey",
            "email": "cnussey2r@examiner.com",
            "gender": "Genderfluid",
            "ip_address": "33.102.124.105"
        },
        [
            "id", 100,
            "first_name", "Corrina",
            "last_name", "Nussey",
            "email", "cnussey2r@examiner.com",
            "gender", "Genderfluid",
            "ip_address", "33.102.124.105"
        ],
        {
            "id": 102,
            "first_name": "Norrie",
            "last_name": "Kezourec",
            "email": "nkezourec2t@zimbio.com",
            "gender": "Polygender",
            "ip_address": "152.189.93.138"
        }];

        const result = problem6(users);
        assert.isArray(result);

    });
});

