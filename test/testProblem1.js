const assert = require('chai').assert;
const { describe } = require('mocha');

const users = require('../users');
const problem1 = require('../problem1');

describe('Problem1', () => {

    it('Function should return user object with id 100', () => {
        const result = problem1(users, 100);
        const expected = {
            id: 100,
            first_name: 'Corrina',
            last_name: 'Nussey',
            email: 'cnussey2r@examiner.com',
            gender: 'Genderfluid',
            ip_address: '33.102.124.105'
        }

        assert.deepEqual(problem1(users, 100), expected);

        console.log(`${result.first_name} is a ${result.gender} and can be contacted on ${result.email}`);
    });

    it('Function should return empty object when id is not an number', () => {
        assert.isObject(problem1(users, 'Hello'));
        assert.isObject(problem1(users, []));
        assert.isEmpty(problem1(users, 'Hello'));
        assert.isEmpty(problem1(users, []));
    });

    it('Return should be empty when id is invalid', () => {
        assert.isObject(problem1(users, 84567));
        assert.isEmpty(problem1(users, 84567));
    });

    it('Function shouldn\'t throw error when users data is not an array', () => {
        assert.isEmpty(problem1({ id: 100, name: "Test", length: 10 }, 100));
        assert.isObject(problem1({ id: 100, name: "Test", length: 10 }, 100));
        assert.isEmpty(problem1(new String("hello"), 100), 100);
        assert.isObject(problem1(new String("hello"), 100), 100);

    });

    it('Function shouldn\'t throw error when users data is not passed', () => {
        assert.isEmpty(problem1([], 100));
        assert.isObject(problem1([], 100));
        assert.isEmpty(problem1());
        assert.isObject(problem1());
    });

    it('Function shouldn\'t throw error when users data is not in proper array of object format', () => {

        const users = [{
            "id": 100,
            "first_name": "Corrina",
            "last_name": "Nussey",
            "email": "cnussey2r@examiner.com",
            "gender": "Genderfluid",
            "ip_address": "33.102.124.105"
        },
        [
            "id", 100,
            "first_name", "Corrina",
            "last_name", "Nussey",
            "email", "cnussey2r@examiner.com",
            "gender", "Genderfluid",
            "ip_address", "33.102.124.105"
        ],
        {
            "id": 102,
            "first_name": "Norrie",
            "last_name": "Kezourec",
            "email": "nkezourec2t@zimbio.com",
            "gender": "Polygender",
            "ip_address": "152.189.93.138"
        }];

        const result = problem1(users, 100);
        assert.isObject(result);

    });
});