const assert = require('chai').assert;
const { describe } = require('mocha');

const users = require('../users');
const problem5 = require('../problem5');

describe('problem5', () => {

    it('Function should list male user names & emails', () => {
        const result = problem5(users);
        console.log(`The total no. of male users are: ${result.length}`);
    });

    it('Function should return empty array when users is not passed', () => {
        assert.isArray(problem5());
        assert.isEmpty(problem5());
    });

    it('Return shouldn\'t be affected with more arguments', () => {
        const result = problem5(users);
        assert.deepEqual(problem5(users, 5478415, 'Dumped string'), result);
    });

    it('Function shouldn\'t throw error when users data is not an array', () => {
        assert.isEmpty(problem5({ id: 33, name: "Test", length: 10 }));
        assert.isArray(problem5({ id: 33, name: "Test", length: 10 }));
        assert.isEmpty(problem5(new String("Mountblue")));
        assert.isArray(problem5(new String("mountblue")));
    });

    it('Function shouldn\'t throw error when users data is not in proper array of object format', () => {

        const users = [{
            "id": 100,
            "first_name": "Corrina",
            "last_name": "Nussey",
            "email": "cnussey2r@examiner.com",
            "gender": "Genderfluid",
            "ip_address": "33.102.124.105"
        },
        [
            "id", 100,
            "first_name", "Corrina",
            "last_name", "Nussey",
            "email", "cnussey2r@examiner.com",
            "gender", "Genderfluid",
            "ip_address", "33.102.124.105"
        ],
        {
            "id": 102,
            "first_name": "Norrie",
            "last_name": "Kezourec",
            "email": "nkezourec2t@zimbio.com",
            "gender": "Polygender",
            "ip_address": "152.189.93.138"
        }];

        const result = problem5(users);
        assert.isArray(result);

    });
});

