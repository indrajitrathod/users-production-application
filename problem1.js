// function which iterate through users and find a user details by its id
const userInfoById = (users, id) => {
    let requiredUser = {};

    if (Array.isArray(users) && typeof id === 'number') {

        for (const user of users) {

            if (user != undefined && typeof user === 'object' && !Array.isArray(user)) {

                if (user.id != undefined && user.id === id) {
                    requiredUser = user;
                }

            }
        }
    }
    return requiredUser;
}

module.exports = userInfoById;

