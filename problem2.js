let users = require('./users');

// function which iterate through users and last user details
const lastUserInfo = (users, id) => {
    let lastUser = {};

    if (Array.isArray(users)) {
        let lastIndex = users.length - 1;

        if (typeof users[lastIndex] === 'object') {
            lastUser = users[lastIndex];
        }

    }
    return lastUser;
}

module.exports = lastUserInfo;

