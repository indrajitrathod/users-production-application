// The function returns the usernames sorted in alphabetic order
const findMaleUsers = (users) => {
    let maleUsers = [];

    if (Array.isArray(users) && users.length) {

        for (const user of users) {

            if (typeof user === 'object' && !Array.isArray(user) && user.gender === 'Male') {
                maleUsers.push({
                    name: `${user.first_name} ${user.last_name}`,
                    email: user.email
                });
            }
        }
    }
    return maleUsers;
}

module.exports = findMaleUsers;